# SWARM APP LIFECYCLE
### USING SECRETS WITH LOCAL DOCKER
Using secrets locally with compose is not secure. It basically bind-mounts with -v at runtime the used file in the container. This is like mentioned not secure, but is a way to get around this problem and allows to develop with the same process and the same environment variables secret information that would be used in production. Difference is they will be used locally instead, thus allowing to develop using the same launch scripts and the same way to get the env variables into the container just like in swarm. This offers the possibility to match the production environment as much as possible locally. Docker-compose 11 is minimum required for this to work. Important: This will only work with file based secrets not with **external:**

---
### FULL APP LIFECYCLE WITH COMPOSE: SINGLE COMPOSE DESIGN
REFS: swarm-stack-3
- use 4 files
  - docker-compose.yml
  - docker-compose.override.yml
  - docker-compose.test.yml
  - docker-compose.prod.yml
- Using docker-compose up, the docker-compose.yml file will be used and then the override file, automatically.
- In the docker-compose.yml are base infos
- Then it will look for the override file.
- For testing you need to specify the files with -f when using docker-compose up. Always define the base file first.
- For production
  - This works a little bit different. Instead of docker-compose up, use docker-compose config, but once again you have to specify the files with -f.
  - This will push both files into a single compose-file equivalent. Use for example with **> output.yml**

---
### SERVICE UPDATES
- Provides rolling replacement of tasks/containers in a service
- Limits downtime
- Will replace containers for most changes
- Many cli options to control the update
- Includes rollback and healthcheck options
- Also has scale & rollback subcommand for quicker access
- A stack deploy, when pre-existing, will issue service updates

##### SWARM UPDATE EXAMPLES
- Update the image used to a newer version
  - docker service update --image myapp:1.2.1 <servicename>
- Add an environment variable and remove a port
  - docker service update --env-add NODE_ENV=production --publish-rm 8080
- Change number of replicas of two services
  - docker service scale web=8 api=6
- Change port
  - docker service update --publish-rm 8080 --publish-add 9090:80
- Rebalance load
  - docker service update --force

##### SWARM UPDATES IN STACK FILES
- Same command. Just edit the yml file, Then
  - docker stack deploy -c file.yml <stackname>

---
### DOCKER HEALTCHECK
- Supported in dockerfile, compose yaml, docker run and swarm
- Docker engine will exec's the command in the container (e.g. curl localost)
- It expects exit 0 (OK) or exit 1 (Err)
- 3 container states: starting, healthy, unhealthy
- Not a external monitoring replacement

##### CONTAINERS
- healthcheck status in docker container ls
- Check last 5 healtchecks with docker container inspect
- Docker run does nothing with healtchecks
- Services will replace tasks if they fail
- Service updatas wait for them before continuing

###### HEALTCHECK DOCKR RUN EXAMPLE
```docker
docker run \
 --health-cmd="curl -f localhost:9200/_cluster/health || false" \
 --health-interval=5s \
 --health-retries=3 \
 --health-timeout=2s \
 --health-start-period=15s \
 elasticsearch:2
```

###### HEALTCHECK DOCKERFILE EXAMPLES
- Options for healtcheck command
  - --interval=DURATION (default:30s)
  - --timeout=DURATION (default:30s)
  - --start-period=DURATION (default:3s) (17.09+)
  - --retries=N (default:3)
- Basic command using default options
```dockerfile
HEALTCHECK curl -f localhost/ || false"
```

- custom options with command
```dockerfile
HEALTCHECK --timeout=2s --interval=3s --retries=3 \
 CMD curl -f localhost/ || exit 1"
```

- Healtcheck in nginx DOCKERFILE
  - Static website running, just test default url
  ```dockerfile
    FROM nginx:1.13
    HEALTCHECK --timeout=3s --interval=30s \
     CMD curl -f localhost/ || exit 1"
  ```

- Healtcheck in postgres DOCKERFILE
  - Use postgre utility to test for ready state
  ```dockerfile
    FROM postgres

    # specify real user with -U to prevent errors in log
    HEALTCHECK --timeout=3s --interval=5s \
     CMD pg_isready -U postgres || exit 1"
  ```

- Healtcheck in compose files
```dockerfile
version: 2.1 #minimum healtcheck
services:
  web:
    image: nginx
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost"]
      interval: 1m30s
      timeout: 10s
      retries: 3
      start_period: 40s #minimum 3.4 version
