# NETWORK
- Each container connected to a private virtual network 'bridge'
- Each virtual network routes through NAT firewall on host IP
- All containers on a virtual network can talk to each other without -p
- Best practices is to create a new virtual network for each app:
  - network 'my_web_app' for mysql and php/apache containers
  - network 'my_api' for mongo and nodejs containers

---
### TRAFFIC FLOW AND FIREWALLS
How docker networks move packets in and out
![Docker Network](./img/docker_network.png)

---

### DOCKER NETWORK CLI MANAGEMENT
- docker network ls
- docker network inspect
- docker network create --driver
  - spawns new virtual network to attach containers
- docker network connect
- docker network disconnect

---
### DNS
Docker daemon has a built-in DNS server that containers use by default if custom networks are used

---
