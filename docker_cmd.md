# DOCKER
---
### COMMANDS
- sudo usermod -aG docker user
- docker version
- docker info
- docker container run --publish 80:80 --detach --name my_name nginx
  - pulls latest image from docker hub, then starts a new container from that image
  - **run** starts always a new container, **start** starts an existing one
  - **publish** opens port 80 on the host and routes the traffic to the container ip port 80
  - **detach** tells docker to run in the background
  - **name** assigns one name to the container
- docker container ps
- docker container ls
- docker container ls -a
- docker container stop *container-id*
- docker container logs *container-name*
- docker container top *container-name*
- docker container rm *container-ids*/*container-names*
- docker top *container-name*
  - list running processes in specific container
- docker container inspect
  - details of one container config (metadata)
  - **--format** to format output
- docker container stats
  - performance stats for all containers
- docker container port *container-name*
- docker cp <containerId>:/file/path/within/container /host/path/target

##### WHAT HAPPENS ON "docker container run"
1. Looks for image locally in image cache, if it doesn't find anything, it will look in a remote repository **(default is Docker Hub)** and download the latest version if no version is specified **(nginx:latest)**
2. Once it has an image ready to go it's going to start up a new container based on that image
3. It will give it a virtual IP on a private network inside the docker engine
4. Without the publish command it wont open up any ports
5. Starts the container using the CMD in the image dockerfile

```docker
docker container run --publish 8080:80 --name webhost -d nginx:1.11 nginx -T
```
---
### GETTING A SHELL INSIDE CONTAINERS
No ssh needed
- docker container run -it
  - start new container interactively
- docker container exec -it
  - run additional command in existing container

###### RUN COMMAND IN NEW CONTAINER
```docker
docker container run -it --name nginx nginx bash
```
The bash at the end changes the default CMD. In this case it will open a terminal inside the container. After exiting the container will stop to run, because container only run as long as the CMD given.
###### RESTART
```docker
docker container run -ai ubuntu
```
###### RUN COMMAND IN RUNNING CONTAINER
```docker
docker container EXEC -it mysql bash
```
Will connect with a bash shell in the container. Even on exit the container will continue to run, because the exec command adds an additional process and will not affect the root process.
###### RESTART
```docker
docker container run -ai ubuntu
```
---
