# DOCKER COMPOSE

### WHY

- Configure relationships between containers
- Save our docker container run settings in easy-to-read file
- Create one-liner developer env startups

##### COMPRISED OF 2 SEPARATED BUT RELATED THINGS
1. YAML-formatted file that describes our solution options for:
  - containers
  - networks
  - volumes
2. A CLI tool used for local local dev/test automation with those YAML files
---
### CLI
Ideal for local development and test
- docker-compuse up
  - setup volumes/networks and start all containers
- docker-compose down
  - stop all containers and remove cont/vol/net
- Usual procedure on a project with Docker file and docker-compose.yml would be
  - Clone project
  - docker-compose up

---
### USING COMPOSE TO BUILD

- compose can build custom images
- if not found in cache with **docker-compose up**
- Rebuild with **docker-compose build** or **docker-compose up --build**
- Great for complex builds with lots of vars or buld args
