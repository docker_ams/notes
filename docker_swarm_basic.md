# SWARM BASIC

### SWARM MODE: BUILT-IN ORCHESTRATION

- Is a clustering solution built inside docker

![swarm](./img/docker_swarm.png)
![swarm](./img/docker_swarm2.png)
![swarm](./img/docker_swarm3.png)
![swarm](./img/docker_swarm4.png)

---

### CREATE SERVICE AND SCALE IT LOCALLY

- docker swarm init
  - PKI and security automation
    - root signing cert for swarm
    - cert is issued for first manager
    - join tokens created
  - Raft database created to store root CA, configs, secrets
    - Encrypted by default on disk (1.13+)
    - NO other key/value system needed to hold orchestration/secrets
    - replicates logs amongst managers through TLS 'control-plane'
- docker node ls
- docker service create
- docker service ls
- docker service ps
- docker service update <ID> --replicas

---
### CREATE A SWARM NODE CLUSTER
##### HOST OPTIONS
1. play-with-docker.com
2. docker-machine + VirtualBox
3. Roll own
  1. docker-machine provision on Amazon, Azure, Google etc. and isntall docker with get.docker.com

---
##### CMDS
- docker-machine create <name>
- docker-machine ssh <name>
- docker-machine env <name>
- docker swarm join ...
- docker node update --role manager <worker-name> <---- manager
- docker swarm join-token manager
- docker service create --replicas <n> <cmd> [args]
- docker service ls  <-- show current services
- docker service ps <ps-name>  <-- show processes running of a specific service
- docker node ls  <-- show list of nodes
- docker node ps  <-- show processes running on this node, add nodename arg to see ps running on other nodes

---
### BASIC FEATURES
##### OVERLAY MULTI-HOST NETWORKING
- Choose docker network create --driver overlay when creating a network, will create a swarm wide bridge network
- For container to container traffic inside a single swarm
- Each service can be connected to multiple networks (eg. frontend backend)
- difference between bridge and overlay
  - bridge only knows of the server it's on, controlled by the Docker Engine on that machine only.Overlay is simply a multi-server bridge network, controlled by Swarm managers and set across all the nodes.

---
### ROUTING MESH
![routing_mesh](./img/docker_routing_mesh.png)
![routing_mesh](./img/docker_routing_mesh2.png)

- this is statless load balancing
- this LB is at OSI layer 3(TCP), not Layer 4 (dns)
- overcome limitation with:
  - nginx or HAProxy LB proxy

---
### STACKS
##### PRODUCTION GRADE COMPOSE
- Stacks accepts compose files (version 3)
- Use docker stacker deploy instead of docker srvcie create
- Stacks manages all objects, including overlay network per stack. Adds stack name to start of theri name
- New **deploy:** key, can't use **build:**
- On local machine compose ignores **deploy:** on swarm **build:** is ignored
- docker-compose cli not needed on swarm server
![routing_mesh](./img/docker_swarm_stack.png)
- docker stack deploy [-c]
- docker stack ls
- docker stack ps <name>
- docker stack services <name>
- docker container ls
- always use the same compose file to make changes to the stack, this is best practice, don't change with the cli. after changing the file reuse the docker stack deploy command.

---
##### SECRETS STORAGE
- Easiest "secure" soution for storing secrets in swarm
- Supports generic strings or binary content up to 500kb in size
- Doesn't require apps to be rewritten
- As of docker 1.13 swarm Raft DB is encrypted on disk
- Only stored on manager nodes
- Default is managers and workers "control plane" is TLS + Mutual Auth
- Secrets first stored in swarm, then assigned to a servcie(s)
- Only containers in assigned service(s) can see them
- Look like file, but are actually in-memory fs
  - /run/secrets/<secret_name>
- Local docker-compose can use file-based secrets, but not secure

###### SECRETS WITH SWARM SERVICES
- docker secret create <name> <file>
- echo "mystring" | docker secret create <name> -
- docker secret ls
- docker service create --name psql --secret psql_user --secret psql_pass -e POSTGRES_PASSWORD_FILE=/run/secrets/psql_pass -e POSTGRES_USER_FILE=/run/secrets/psql_user postgres
- docker secret rm

---
###### SECRETS WITH SWARM STACKS
- compose file needs to be at least 3.1
- docker stack -c docker-compose.yml <name>
- docker stack rm mydb <-- removes also secrets

always pin versions in production
