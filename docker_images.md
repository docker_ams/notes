# IMAGES

### WHAT'S IN AN IMAGE (AND WHAT ISN'T)

- App binaries and dependencies
- Metadata about the image data and how to run the image
- Not a complete OS. No kernel, kernel modules (e.g. drivers)
- Small as one file
- Or as big as a distro

---

### PUSHING AND RETAGGING

- docker login
- docker logout
- docker image push my_acc/my_image
- docker image tag container:tag my_acc/my_image

After logging in an 'auths' key will be added to the docker config file in the home dir.

### BUILD

- docker image build -t my_tag .
  - will work through each step of the Dockerfile. Each step is a line in the Dockerfile and the hash at the end of the step , is the hash docker keeps in the build cache. If on the next build he line hasn't been changed, then docker wont rerun it and save time. Every line after the changed line has to be rebuilt as well.
