# LIFETIME AND PERSISTEND DATA

### DATA VOLUMES

Survive even after deletion of container.
- docker volume ls
- docker volume prune <---- removes all unused volumes
- **-v name:/path/to/dest** when running new container specifies volume name. can also be used to create volume

---
### BIND MOUNTING

- Maps a host file or directory to a container file or directory
- Two locations pointing to the same file
- Can't use in docker file
  - ... run -v /users/user/path:/path/container
  - ... run -v $(pwd):/path/container

### UPDATE DATABASE

- create container and bind mount volume
- when updating stop the container
- create new container with new version (no major update) and add volume
- check logs
