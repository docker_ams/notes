# DOCKER HUB

# DOCKER STORE

# DOCKER CLOUD
[Link to cloud doc](https://cloud.docker.com/)

# DOCKER REGISTRY
[Link to registry doc](https://docs.docker.com/registry/configuration/#list-of-configuration-options)

Private docker registry

- https server
- "Secure by default": Docker won't talkt to registry without https
- Except localhost
- For remote self-signed TLS, enable "insercure-registry" in engine

![reg](./img/docker_registry.png)

### SECURE DOCKER REGISTRY WITH TLS AND AUTH
##### RUN A SECURED REGISTRY CONTAINER
```bash
# generate ssl certificate in linux
# If you are running the registry locally, be sure to use your host name as the CN. e.g. 127.0.0.1
mkdir -p certs
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt
# To get the docker daemon to trust the certificate, copy the domain.crt file.
mkdir /etc/docker/certs.d
mkdir /etc/docker/certs.d/127.0.0.1:5000
cp $(pwd)/certs/domain.crt /etc/docker/certs.d/127.0.0.1:5000/ca.crt
# restart docker daemon
# The /dev/null part is to avoid the output logs from docker daemon.
pkill dockerd
dockerd > /dev/null 2>&1 &
# Now we have an SSL certificate and can run a secure registry.
# Run container with SSL cert and key files available
mkdir registry-data
docker run -d -p 5000:5000 --name registry \
  --restart unless-stopped \
  -v $(pwd)/registry-data:/var/lib/registry -v $(pwd)/certs:/certs \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  registry
# Access the secure registry
docker pull hello-world
docker tag hello-world 127.0.0.1:5000/hello-world
docker push 127.0.0.1:5000/hello-world
docker pull 127.0.0.1:5000/hello-world
```
---
##### USE BASIC AUTH WITH A SECURED REGISTRY IN LINUX
The registry server and the Docker client support basic authentication over HTTPS. The server uses a file with a collection of usernames and encrypted passwords. The file uses Apache’s htpasswd.

```bash
# Create the password file user 'moby' pw 'gordon'
# The options are:
# –entrypoint Overwrite the default ENTRYPOINT of the image
# -B Use bcrypt encryption (required)
# -b run in batch mode
# -n display results
mkdir auth
docker run --entrypoint htpasswd registry:latest -Bbn moby gordon > auth/htpasswd
# Verify the entries have been written
cat auth/htpasswd
# Run an authenticated secure registry
# As before, we’ll remove the existing container and run a new one with authentication configured:
# The options for this container are:
# -v $(pwd)/auth:/auth - mount the local auth folder into the container, so the registry server can access htpasswd file;
# -e REGISTRY_AUTH=htpasswd - use the registry’s htpasswd authentication method;
# -e REGISTRY_AUTH_HTPASSWD_REALM='Registry Realm' - specify the authentication realm;
# -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd - specify the location of the htpasswd file.
docker kill registry
docker rm registry
docker run -d -p 5000:5000 --name registry \
  --restart unless-stopped \
  -v $(pwd)/registry-data:/var/lib/registry \
  -v $(pwd)/certs:/certs \
  -v $(pwd)/auth:/auth \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -e REGISTRY_AUTH=htpasswd \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
  -e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" \
  registry

# With basic authentication, users cannot push or pull from the registry unless they are authenticated.
# If you try and pull an image without authenticating, you will get an error
# Logging in to the registry is the same docker login command you use for Docker Store,
# specifying the registry hostname:
docker login 127.0.0.1:5000
```
---
### PRIVATE DOCKER REGISTRY WITH SWARM
- works the same as localhost
- Because of routing mesh, all nodes can see 127.0.0.1:5000
- You have do decide how to store images (volume driver)
- All nodes must be able to access images
- Optional: Use a hosted SaaS reigstry
  - https://github.com/veggiemonk/awesome-docker#hosting-images-registries

- docker service create --name registry -p 5000:5000 registry
- check registry with <registry-url>/v2/_catalog
- docker tag hello-world 127.0.0.1:5000/hello-world
- docker push 127.0.0.1:5000/hello-world
- on rechecking the url the repo will be shown
